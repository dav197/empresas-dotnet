﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas.Database
{
    public class Movie
    {
        public Movie()
        {

        }

        public List<int> Votes { get; set; }
        public string Director { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Genre { get; set; }
        public List<string> Actors { get; set; }
    }
}
