﻿using Microsoft.EntityFrameworkCore;

namespace empresas.Database
{
    public class Context : DbContext
    {
        public Context() : base()
        {

        }
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies { get; set; }
    }
}
