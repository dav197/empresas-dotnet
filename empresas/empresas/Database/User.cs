﻿namespace empresas.Database
{
    public class User
    {
        public User()
        {

        }

        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
        public string Birthday { get; set; }
        public bool IsDisabled { get; set; }
    }
}
