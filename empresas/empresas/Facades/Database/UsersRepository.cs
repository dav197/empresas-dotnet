﻿using empresas.Database;
using empresas.Facades.Database.Interfaces;
using empresas.Models.Requests.Users;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace empresas.Facades.Database
{
    public class UsersRepository : IUsersRepository
    {
        public async Task EditUserAsync(EditUserRequest request, bool isAdmin, bool isDisable)
        {
            using (var ctx = new Context())
            {
                var userInDB = ctx.Users
                    .Where(u => u.Login.Equals(request.Login))
                    .FirstOrDefault();

                ctx.Entry(userInDB).State = EntityState.Detached;

                var user = new User()
                {
                    Birthday = request.Birthday,
                    IsAdmin = isAdmin,
                    IsDisabled = isDisable,
                    Login = request.Login,
                    Name = request.Name,
                    Password = BCrypt.Net.BCrypt.HashPassword(request.Password)
                };

                ctx.Entry(user).State = EntityState.Modified;

                await ctx.SaveChangesAsync();
            }
        }

        public async Task CreateUserAsync(CreateUserRequest request, bool isAdmin)
        {
            using (var ctx = new Context())
            {
                var user = new User()
                {
                    Birthday = request.Birthday,
                    IsAdmin = isAdmin,
                    IsDisabled = false,
                    Login = request.Login,
                    Name = request.Name,
                    Password = BCrypt.Net.BCrypt.HashPassword(request.Password)
                };

                ctx.Users.Add(user);
                await ctx.SaveChangesAsync();
            }
        }

        public User GetUser(string login)
        {
            using (var ctx = new Context())
            {
                return ctx.Users.Where(u => u.Login.Equals(login)).FirstOrDefault();
            }
        }
    }
}
