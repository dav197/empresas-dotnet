﻿using empresas.Database;
using empresas.Facades.Database.Interfaces;
using empresas.Models.Requests.Movies;
using System.Linq;
using System.Threading.Tasks;

namespace empresas.Facades.Database
{
    public class MoviesRepository : IMoviesRepository
    {
        public async Task CreateMovieAsync(CreateMovieRequest request)
        {
            using (var ctx = new Context())
            {
                var movie = new Movie()
                {
                    Actors = request.Actors,
                    Director = request.Director,
                    Genre = request.Genre,
                    Name = request.Name,
                    Votes = request.Votes
                };

                ctx.Movies.Add(movie);
                await ctx.SaveChangesAsync();
            }
        }

        public Movie GetMovie(long id)
        {
            using (var ctx = new Context())
            {
                return ctx.Movies.Where(m => m.Id == id).FirstOrDefault();
            }
        }
    }
}
