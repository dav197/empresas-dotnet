﻿using empresas.Database;
using empresas.Models.Requests.Movies;
using System.Threading.Tasks;

namespace empresas.Facades.Database.Interfaces
{
    public interface IMoviesRepository
    {
        Task CreateMovieAsync(CreateMovieRequest request);

        Movie GetMovie(long id);
    }
}
