﻿using empresas.Database;
using empresas.Models.Requests.Users;
using System.Threading.Tasks;

namespace empresas.Facades.Database.Interfaces
{
    public interface IUsersRepository
    {
        Task EditUserAsync(EditUserRequest request, bool isAdmin, bool isDisable);

        Task CreateUserAsync(CreateUserRequest request, bool isAdmin);

        User GetUser(string login);
    }
}
