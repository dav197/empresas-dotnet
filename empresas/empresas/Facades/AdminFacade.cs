﻿using empresas.Facades.Database.Interfaces;
using empresas.Facades.Interfaces;
using empresas.Models.Requests.Users;
using empresas.Models.Responses.Admin;
using System.Threading.Tasks;

namespace empresas.Facades
{
    public class AdminFacade : IAdminFacade
    {
        private const string SUCCESS = "Sucesso";

        private readonly IUsersRepository _usersRepository;

        public AdminFacade(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<DefaultUsersResponse> CreateAdminAsync(CreateUserRequest request)
        {
            await _usersRepository.CreateUserAsync(request, true);

            return new DefaultUsersResponse()
            {
                Response = SUCCESS
            };
        }

        public async Task<DefaultUsersResponse> EditAdminAsync(EditUserRequest request)
        {
            await _usersRepository.EditUserAsync(request, true, false);

            return new DefaultUsersResponse()
            {
                Response = SUCCESS
            };
        }

        public async Task<DefaultUsersResponse> DeleteAdminAsync(EditUserRequest request)
        {
            await _usersRepository.EditUserAsync(request, true, true);

            return new DefaultUsersResponse()
            {
                Response = SUCCESS
            };
        }

    }
}
