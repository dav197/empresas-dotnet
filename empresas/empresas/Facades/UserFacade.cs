﻿using empresas.Facades.Database.Interfaces;
using empresas.Facades.Interfaces;
using empresas.Models.Requests.Users;
using empresas.Models.Responses.Admin;
using System.Threading.Tasks;

namespace empresas.Facades
{
    public class UserFacade : IUserFacade
    {
        private const string SUCCESS = "Sucesso";

        private readonly IUsersRepository _usersRepository;

        public UserFacade(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<DefaultUsersResponse> CreateUserAsync(CreateUserRequest request)
        {
            await _usersRepository.CreateUserAsync(request, false);

            return new DefaultUsersResponse()
            {
                Response = SUCCESS
            };
        }

        public async Task<DefaultUsersResponse> EditUserAsync(EditUserRequest request)
        {
            await _usersRepository.EditUserAsync(request, false, false);

            return new DefaultUsersResponse()
            {
                Response = SUCCESS
            };
        }

        public async Task<DefaultUsersResponse> DeleteUserAsync(EditUserRequest request)
        {
            await _usersRepository.EditUserAsync(request, false, true);

            return new DefaultUsersResponse()
            {
                Response = SUCCESS
            };
        }
    }
}
