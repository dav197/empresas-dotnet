﻿using empresas.Database;
using empresas.Facades.Database.Interfaces;
using empresas.Facades.Interfaces;
using empresas.Models.Requests.Movies;
using empresas.Models.Responses.Movies;
using System;
using System.Threading.Tasks;

namespace empresas.Facades
{
    public class MoviesFacade : IMoviesFacade
    {
        private const string SUCCESS = "Sucesso";

        private readonly IUsersRepository _usersRepository;
        private readonly IMoviesRepository _moviesRepository;

        public MoviesFacade(IUsersRepository usersRepository, IMoviesRepository moviesRepository)
        {
            _usersRepository = usersRepository;
            _moviesRepository = moviesRepository;
        }

        public async Task<DefaultMoviesResponse> CreateMovieAsync(CreateMovieRequest request)
        {
            var userInDB = _usersRepository.GetUser(request.Login);

            if (userInDB.IsAdmin)
            {
                await _moviesRepository.CreateMovieAsync(request);

                return new DefaultMoviesResponse()
                {
                    Response = SUCCESS
                };
            }

            throw new InvalidOperationException("Somente administradores podem adicionar novos filmes");
        }

        public async Task<DefaultMoviesResponse> VoteMovieAsync(VoteMovieRequest request)
        {
            var userInDB = _usersRepository.GetUser(request.Login);

            if (!userInDB.IsAdmin)
            {
                return new DefaultMoviesResponse()
                {
                    Response = SUCCESS
                };
            }

            throw new InvalidOperationException("Somente usuários comuns podem adicionar votar");
        }

        public async Task<Movie> DetailMovieAsync(long movieId)
        {
            var movie = _moviesRepository.GetMovie(movieId);

            return movie;
        }
    }
}
