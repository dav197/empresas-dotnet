﻿using empresas.Facades.Database.Interfaces;
using empresas.Facades.Interfaces;
using empresas.Models.Requests.Login;
using empresas.Models.Responses.Login;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace empresas.Facades
{
    public class LoginFacade : ILoginFacade
    {
        private const string LOGIN = "Login";

        private readonly IUsersRepository _usersRepository;

        public LoginFacade(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<LoginResponse> LoginAsync(LoginRequest request)
        {
            var userInDB = _usersRepository.GetUser(request.Login);

            if (BCrypt.Net.BCrypt.Verify(request.Password, userInDB.Password))
            {
                return GetToken(request);
            }
            throw new InvalidOperationException("Senha errada");
        }

        private static LoginResponse GetToken(LoginRequest request)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("0800fc577294c34e0b28ad2839435945");

            var subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(LOGIN, request.Login)
            });

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = subject,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Expires = DateTime.UtcNow.AddMinutes(60)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new LoginResponse()
            {
                Token = tokenHandler.WriteToken(token)
            };
        }
    }
}
