﻿using empresas.Database;
using empresas.Models.Requests.Movies;
using empresas.Models.Responses.Movies;
using System.Threading.Tasks;

namespace empresas.Facades.Interfaces
{
    public interface IMoviesFacade
    {
        Task<DefaultMoviesResponse> CreateMovieAsync(CreateMovieRequest request);

        Task<DefaultMoviesResponse> VoteMovieAsync(VoteMovieRequest request);

        Task<Movie> DetailMovieAsync(long movieId);
    }
}
