﻿using empresas.Models.Requests.Users;
using empresas.Models.Responses.Admin;
using System.Threading.Tasks;

namespace empresas.Facades.Interfaces
{
    public interface IUserFacade
    {
        Task<DefaultUsersResponse> CreateUserAsync(CreateUserRequest request);
        Task<DefaultUsersResponse> EditUserAsync(EditUserRequest request);
        Task<DefaultUsersResponse> DeleteUserAsync(EditUserRequest request);
    }
}
