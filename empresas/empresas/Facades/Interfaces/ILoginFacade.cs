﻿using empresas.Models.Requests.Login;
using empresas.Models.Responses.Login;
using System.Threading.Tasks;

namespace empresas.Facades.Interfaces
{
    public interface ILoginFacade
    {
        Task<LoginResponse> LoginAsync(LoginRequest request);
    }
}
