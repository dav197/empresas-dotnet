﻿using empresas.Models.Requests.Users;
using empresas.Models.Responses.Admin;
using System.Threading.Tasks;

namespace empresas.Facades.Interfaces
{
    public interface IAdminFacade
    {
        Task<DefaultUsersResponse> CreateAdminAsync(CreateUserRequest request);
        Task<DefaultUsersResponse> EditAdminAsync(EditUserRequest request);
        Task<DefaultUsersResponse> DeleteAdminAsync(EditUserRequest request);
    }
}
