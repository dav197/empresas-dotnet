﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas.Models.Requests.Movies
{
    public class ListMoviesRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("director")]
        public string Director { get; set; }

        [JsonProperty("genre")]
        public string Genre { get; set; }

        [JsonProperty("actor")]
        public string Actor { get; set; }

        [JsonProperty("pagination")]
        public int Pagination { get; set; }
    }
}
