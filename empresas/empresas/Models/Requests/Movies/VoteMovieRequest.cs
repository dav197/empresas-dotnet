﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas.Models.Requests.Movies
{
    public class VoteMovieRequest
    {
        [JsonProperty("movieId")]
        public string MovieId { get; set; }

        [JsonProperty("vote")]
        public int Vote { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }
    }
}
