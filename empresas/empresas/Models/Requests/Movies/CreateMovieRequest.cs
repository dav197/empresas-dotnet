﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace empresas.Models.Requests.Movies
{
    public class CreateMovieRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("director")]
        public string Director { get; set; }

        [JsonProperty("votes")]
        public List<int> Votes { get; set; }

        [JsonProperty("genre")]
        public string Genre { get; set; }

        [JsonProperty("genre")]
        public List<string> Actors { get; set; }

        [JsonProperty("login")]
        public string Login { get; set; }
    }
}
