﻿using Newtonsoft.Json;

namespace empresas.Models.Requests.Login
{
    public class LoginRequest
    {
        [JsonProperty("login")]
        public string Login { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
