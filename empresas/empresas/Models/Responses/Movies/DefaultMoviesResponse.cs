﻿using Newtonsoft.Json;

namespace empresas.Models.Responses.Movies
{
    public class DefaultMoviesResponse
    {
        [JsonProperty("response")]
        public string Response { get; set; }
    }
}
