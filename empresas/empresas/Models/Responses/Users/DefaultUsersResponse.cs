﻿using Newtonsoft.Json;

namespace empresas.Models.Responses.Admin
{
    public class DefaultUsersResponse
    {
        [JsonProperty("response")]
        public string Response { get; set; }
    }
}
