﻿using Newtonsoft.Json;

namespace empresas.Models.Responses.Login
{
    public class LoginResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
