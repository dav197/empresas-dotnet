﻿using empresas.Facades.Interfaces;
using empresas.Models.Requests.Users;
using empresas.Models.Responses.Admin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace empresas.Controllers
{
    [Route("api/[controller]")]
    public class AdminController : ControllerBase
    {
        private const string REGISTER = "Cadastro de um novo administrador";
        private const string EDIT = "Edita um administrador";
        private const string DELETE = "Deleta um administrador";
        private const string LIST = "Lista todos os usuários que não são administradores";

        private readonly IAdminFacade _adminFacade;

        /// <summary>
        /// Ctor for adminFacade injection
        /// </summary>
        /// <param name="adminFacade"></param>
        public AdminController(IAdminFacade adminFacade)
        {
            _adminFacade = adminFacade;
        }

        /// <summary>
        /// Register a new admin
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = REGISTER)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(DefaultUsersResponse))]
        [HttpPost]
        public async Task<IActionResult> CreateAdminAsync([FromBody] CreateUserRequest request)
        {
            var response = await _adminFacade.CreateAdminAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Edit an admin
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = EDIT)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(DefaultUsersResponse))]
        [HttpPut, Authorize]
        public async Task<IActionResult> EditAdminAsync([FromBody] EditUserRequest request)
        {
            var response = await _adminFacade.EditAdminAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Delete an admin
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = DELETE)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(DefaultUsersResponse))]
        [HttpDelete, Authorize]
        public async Task<IActionResult> DeleteAdminAsync([FromBody] EditUserRequest request)
        {
            var response = await _adminFacade.DeleteAdminAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// List all user not admin
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = LIST)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(void))]
        [HttpGet, Authorize]
        [Route("list-users")]
        public async Task<IActionResult> ListUsersAsync()
        {
            return Ok();
        }
    }
}
