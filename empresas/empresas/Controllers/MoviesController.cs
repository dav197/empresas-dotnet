﻿using empresas.Database;
using empresas.Facades.Interfaces;
using empresas.Models.Requests.Movies;
using empresas.Models.Responses.Movies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace empresas.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private const string REGISTER = "Cadastro de um novo filme";
        private const string VOTE = "Votar em um filme";
        private const string LIST = "Lista os filmes";
        private const string DETAILS = "Busca detalhes de um filme";

        private readonly IMoviesFacade _moviesFacade;

        /// <summary>
        /// Ctor for moviesFacade injection
        /// </summary>
        /// <param name="moviesFacade"></param>
        public MoviesController(IMoviesFacade moviesFacade)
        {
            _moviesFacade = moviesFacade;
        }

        /// <summary>
        /// Register a new movie
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = REGISTER)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(DefaultMoviesResponse))]
        [HttpPost, Authorize]
        public async Task<IActionResult> CreateMovieAsync([FromBody] CreateMovieRequest request)
        {
            var response = await _moviesFacade.CreateMovieAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Vote for a movie
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = VOTE)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(DefaultMoviesResponse))]
        [HttpPost, Authorize]
        [Route("vote")]
        public async Task<IActionResult> VoteMovieAsync([FromBody] VoteMovieRequest request)
        {
            var response = await _moviesFacade.VoteMovieAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// List all movies
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = LIST)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(ListMoviesRequest))]
        [HttpGet, Authorize]
        public async Task<IActionResult> ListMoviesAsync([FromQuery] ListMoviesRequest request)
        {
            return Ok();
        }

        /// <summary>
        /// List all movies
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = DETAILS)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(Movie))]
        [HttpGet, Authorize]
        [Route("{movieId}")]
        public async Task<IActionResult> DetailMovieAsync([FromRoute] long movieId)
        {
            var response = await _moviesFacade.DetailMovieAsync(movieId);

            return Ok(response);
        }
    }
}
