﻿using empresas.Facades.Interfaces;
using empresas.Models.Requests.Login;
using empresas.Models.Responses.Login;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private const string LOGIN = "Logar um usuário, seja ele administrador ou não";

        private readonly ILoginFacade _loginFacade;

        /// <summary>
        /// Ctor for adminFacade injection
        /// </summary>
        /// <param name="adminFacade"></param>
        public LoginController(ILoginFacade loginFacade)
        {
            _loginFacade = loginFacade;
        }

        /// <summary>
        /// Logs an user in
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = LOGIN)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(LoginResponse))]
        [HttpPost]
        public async Task<IActionResult> LoginAsync([FromBody] LoginRequest request)
        {
            var response = await _loginFacade.LoginAsync(request);

            return Ok(response);
        }
    }
}
