﻿using empresas.Facades.Interfaces;
using empresas.Models.Requests.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;

namespace empresas.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private const string REGISTER = "Cadastro de um novo usuário";
        private const string EDIT = "Edita um usuário";
        private const string DELETE = "Deleta um usuário";

        private readonly IUserFacade _userFacade;

        /// <summary>
        /// Ctor for adminFacade injection
        /// </summary>
        /// <param name="userFacade"></param>
        public UserController(IUserFacade userFacade)
        {
            _userFacade = userFacade;
        }

        /// <summary>
        /// Register a new user
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = REGISTER)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(CreateUserRequest))]
        [HttpPost]
        public async Task<IActionResult> CreateUserAsync([FromBody] CreateUserRequest request)
        {
            var response = await _userFacade.CreateUserAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Edit a user
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = EDIT)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(EditUserRequest))]
        [HttpPut, Authorize]
        public async Task<IActionResult> EditUserAsync([FromBody] EditUserRequest request)
        {
            var response = await _userFacade.EditUserAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="request"></param>
        [SwaggerOperation(Summary = DELETE)]
        [ProducesResponseType(statusCode: StatusCodes.Status200OK, Type = typeof(EditUserRequest))]
        [HttpDelete, Authorize]
        public async Task<IActionResult> DeleteUserAsync([FromBody] EditUserRequest request)
        {
            var response = await _userFacade.DeleteUserAsync(request);

            return Ok(response);
        }

    }
}
