using empresas.Database;
using empresas.Facades;
using empresas.Facades.Database;
using empresas.Facades.Database.Interfaces;
using empresas.Facades.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

namespace empresas
{
    public class Startup
    {
        private const string APPLICATION_NAME = "empresas";
        private const string HEADER_NAME = "Authorization";
        private const string SECURITY_DESCRIPTION = "First of all, generate a JWT using the Login endpoint. " +
                                                    "</BR>Fill the value using the generated token in the following format:</BR> \"Bearer {token}\"";


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IAdminFacade, AdminFacade>();
            services.AddSingleton<IUserFacade, UserFacade>();
            services.AddSingleton<IMoviesFacade, MoviesFacade>();
            services.AddSingleton<ILoginFacade, LoginFacade>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IUsersRepository, UsersRepository>();
            services.AddSingleton<IMoviesRepository, MoviesRepository>();

            services.AddEntityFrameworkSqlServer()
            .AddDbContext<Context>(options =>
              options.UseSqlServer(Configuration["Data:DefaultConnection:Connectionstring"]));

            AddJwtAuthentication(services);
            AddSwagger(services);
        }

        private static void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var version = "1.0";
                var securityScheme = new OpenApiSecurityScheme
                {
                    Description = SECURITY_DESCRIPTION,
                    Name = HEADER_NAME,
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Reference = new OpenApiReference
                    {
                        Id = JwtBearerDefaults.AuthenticationScheme,
                        Type = ReferenceType.SecurityScheme
                    }
                };
                var security = new OpenApiSecurityRequirement
                {
                    {
                        securityScheme,
                        new string[] { }
                    }
                };

                options.SwaggerDoc($"{version}", new OpenApiInfo { Title = APPLICATION_NAME, Version = version });
                options.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, securityScheme);
                options.AddSecurityRequirement(security);
                options.EnableAnnotations();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("./swagger/1.0/swagger.json", "empresas");
                options.RoutePrefix = string.Empty;
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static IServiceCollection AddJwtAuthentication(IServiceCollection services)
        {
            var key = Encoding.ASCII.GetBytes("0800fc577294c34e0b28ad2839435945");

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            return services;
        }
    }
}
